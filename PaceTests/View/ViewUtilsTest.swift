import XCTest
@testable import Pace

class ViewUtilsTest: XCTestCase {
  
  var id: Int!
  var vc: UIViewController!
  var viewUtils: ViewUtils!

  override func setUp() {
    self.id = ViewUtils().id
    self.vc = UIViewController()
    self.viewUtils = ViewUtils()
    _ = vc.view
  }
  
  func testViewDidAppear() {
    viewUtils.indicator(vc.view!, true)
    XCTAssert(vc.view!.viewWithTag(id) != nil)
  }
  
  func testViewDidDisappear() {
    viewUtils.indicator(vc.view!, true)
    XCTAssert(vc.view!.viewWithTag(id) != nil)
    
    viewUtils.indicator(vc.view!, false)
    XCTAssert(vc.view!.viewWithTag(id) == nil)
  }
  
}
