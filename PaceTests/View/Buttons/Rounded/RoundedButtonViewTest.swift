import XCTest
@testable import Pace

class RoundedButtonsViewTest: XCTestCase {
  class TestModel: BaseModel {
    var id: Int
    var text: String
    
    init(id: Int, text: String) {
      self.id = id
      self.text = text
    }
    
  }
  
  func testFrameInit() {
    let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
    let view = RoundedButtonsView.init(frame: frame)
    XCTAssert(view.frame == frame)
  }
  
  func testNibIsIBDesignable() {
    let view = RoundedButtonsView()
    view.subviews.forEach { $0.removeFromSuperview() }
    XCTAssertEqual(view.subviews, [])
    view.prepareForInterfaceBuilder()
    XCTAssertFalse(view.subviews.isEmpty)
  }
  
  func testSetup() {
    let a = TestModel(id: 0, text: "a")
    let b = TestModel(id: 1, text: "b")
    let c = TestModel(id: 2, text: "c")
    let view = RoundedButtonsView()
    view.setup([a, b, c], nil)

    let items = view.items as! [TestModel]
    XCTAssert(items.count == 3)
    XCTAssert(items[0].id == a.id)
    XCTAssert(items[1].id == b.id)
    XCTAssert(items[2].id == c.id)
    XCTAssert(items[0].text == a.text)
    XCTAssert(items[1].text == b.text)
    XCTAssert(items[2].text == c.text)
  }

  func testOnClick() {
    let a = TestModel(id: 0, text: "a")
    let view = RoundedButtonsView()
    view.setup([a]) { XCTAssert(($0 as! TestModel).id == a.id) }
    view.buttons[0].sendActions(for: .touchUpInside)
    
  }
  
}
