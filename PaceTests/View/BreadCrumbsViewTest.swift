import XCTest
@testable import Pace

class BreadCrumbsViewTest: XCTestCase {
  
  func testFrameInit() {
    let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
    let view = BreadCrumbsView.init(frame: frame)
    XCTAssert(view.frame == frame)
  }
  
  func testNibIsIBDesignable() {
    let view = BreadCrumbsView()
    view.subviews.forEach { $0.removeFromSuperview() }
    XCTAssertEqual(view.subviews, [])
    view.prepareForInterfaceBuilder()
    XCTAssertFalse(view.subviews.isEmpty)
  }
  
  func testSetup() {
    let view = BreadCrumbsView()
    view.setup(["a", "b", "c"], nil)
    XCTAssert(view.items[0] == "a")
    XCTAssert(view.items[1] == "b")
    XCTAssert(view.items[2] == "c")
  }
  
  func testRemoveLast() {
    let view = BreadCrumbsView()
    view.setup(["a", "b", "c"], nil)
    XCTAssert(view.items.count == 3)
    
    let list = view.removeLast()
    XCTAssert(list == view.items)
  }
  
  func testOnClick() {
    let view = BreadCrumbsView()
    view.setup(["a", "b", "c"]) { XCTAssert($0 == 1) }
    view.tableView(view.tableView, didSelectRowAt: IndexPath(row: 0, section: 0))
  }
  
  func testHasCorrectCell() {
    let view = BreadCrumbsView()
    view.setup(["a", "b", "c"], nil) 
    let cellA = view.tableView(view.tableView, cellForRowAt: IndexPath(row: 0, section: 0))
    let cellB = view.tableView(view.tableView, cellForRowAt: IndexPath(row: 1, section: 0))
    let cellC = view.tableView(view.tableView, cellForRowAt: IndexPath(row: 2, section: 0))

    XCTAssert(cellA.textLabel!.text! == "a")
    XCTAssert(cellB.textLabel!.text! == "b")
    XCTAssert(cellC.textLabel!.text! == "c")
  }
  
}
