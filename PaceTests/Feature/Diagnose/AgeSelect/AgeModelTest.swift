import XCTest
@testable import Pace

class AgeModelTest: XCTestCase {

  var ageOne: AgeModel!
  var ageTwo: AgeModel!

  override func setUp() {
    ageOne = AgeModel(data: ["id": 0, "text": "a"])
    ageTwo = AgeModel(data: ["id": 1, "text": "b"])
  }

  func testEquatable() {
    XCTAssert(ageOne.id == 0)
    XCTAssert(ageOne.text == "a")

    XCTAssert(ageTwo.id == 1)
    XCTAssert(ageTwo.text == "b")

    XCTAssert(ageOne != ageTwo)
    XCTAssert(ageOne == ageOne)
  }

  func testDescribable() {
    let outputOne = ageOne.description
    XCTAssert(outputOne.contains("class: Age"))
    XCTAssert(outputOne.contains("id: 0"))
    XCTAssert(outputOne.contains("text: a"))

    let outputTwo = ageTwo.description
    XCTAssert(outputOne.contains("class: Age"))
    XCTAssert(outputTwo.contains("id: 1"))
    XCTAssert(outputTwo.contains("text: b"))

    XCTAssert(outputOne != outputTwo)
  }

}
