import XCTest
@testable import Pace

class AgeSelectTest: XCTestCase {

  var vc: AgeSelect!
  
  override func setUp() {
    vc = AgeSelect()
    vc.breadCrumbsView = nil
    vc.roundedButtonsView = nil
    vc.items = []
    vc.crumbs = []
    vc.title = "TestTitle"
    _ = vc.view
  }
  
  func testViewDidLoad() {
    XCTAssert(vc.items.count == 0)
    XCTAssert(vc.crumbs.count == 0)
    XCTAssert(vc.title == "TestTitle")
    //XCTAssert(vc.breadCrumbsView == nil)
    //XCTAssert(vc.roundedButtonsView == nil)
  }
  
  func testViewWillDisappear() {
    vc.crumbs = ["a", "b", "c"]
    XCTAssert(vc.crumbs.count == 3)
  
    vc.viewWillDisappear(false)
    vc.viewDidDisappear(true)
    XCTAssert(vc.crumbs.count == 2)
  }
  
//  func testOnClick() {
//    let age = AgeModel(data: ["id": 0, "text": "a"])
//    vc.onClick(item: age)
//    XCTAssert(vc.crumbs.last! == "a")
//  }
  
}
