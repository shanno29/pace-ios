import XCTest
@testable import Pace

class OptionModelTest: XCTestCase {

  var optionOne: OptionModel!
  var optionTwo: OptionModel!

  override func setUp() {
    optionOne = OptionModel(data: ["id": 0, "text": "a", "caption": "z", "age": 1, "type": 1, "parent": 1, "cases": [], "options": []])
    optionTwo = OptionModel(data: ["id": 1, "text": "b", "caption": "y", "age": 2, "type": 2, "parent": nil, "cases": [], "options": []])
  }

  func testEquatable() {
    XCTAssert(optionOne.id == 0)
    XCTAssert(optionOne.text == "a")
    XCTAssert(optionOne.caption == "z")
    XCTAssert(optionOne.age == 1)
    XCTAssert(optionOne.type == 1)
    XCTAssert(optionOne.parent == 1)
    XCTAssert(optionOne.cases.isEmpty)
    XCTAssert(optionOne.options.isEmpty)

    XCTAssert(optionTwo.id == 1)
    XCTAssert(optionTwo.text == "b")
    XCTAssert(optionTwo.caption == "y")
    XCTAssert(optionTwo.age == 2)
    XCTAssert(optionTwo.type == 2)
    XCTAssert(optionTwo.parent == nil)
    XCTAssert(optionTwo.cases.isEmpty)
    XCTAssert(optionTwo.options.isEmpty)

    XCTAssert(optionOne != optionTwo)
    XCTAssert(optionOne == optionOne)
  }

  func testDescribable() {
    let outputOne = optionOne.description
    XCTAssert(outputOne.contains("class: Option"))
    XCTAssert(outputOne.contains("id: 0"))
    XCTAssert(outputOne.contains("text: a"))
    XCTAssert(outputOne.contains("caption: z"))
    XCTAssert(outputOne.contains("age: 1"))
    XCTAssert(outputOne.contains("type: 1"))
    XCTAssert(outputOne.contains("parent: 1"))
    XCTAssert(outputOne.contains("cases: []"))
    XCTAssert(outputOne.contains("options: []"))

    let outputTwo = optionTwo.description
    XCTAssert(outputOne.contains("class: Option"))
    XCTAssert(outputTwo.contains("id: 1"))
    XCTAssert(outputTwo.contains("text: b"))
    XCTAssert(outputTwo.contains("caption: y"))
    XCTAssert(outputTwo.contains("age: 2"))
    XCTAssert(outputTwo.contains("type: 2"))
    XCTAssert(outputTwo.contains("parent: -1"))
    XCTAssert(outputTwo.contains("cases: []"))
    XCTAssert(outputTwo.contains("options: []"))

    XCTAssert(outputOne != outputTwo)
  }

}
