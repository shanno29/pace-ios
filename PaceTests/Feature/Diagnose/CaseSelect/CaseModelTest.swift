import XCTest
@testable import Pace

class CaseModelTest: XCTestCase {

  var caseOne: CaseModel!
  var caseTwo: CaseModel!

  override func setUp() {
    caseOne = CaseModel(data: ["id": 0, "age": 1, "text": "a", "description": "b", "images": []])
    caseTwo = CaseModel(data: ["id": 1, "age": 2, "text": "d", "description": "e", "images": []])
  }

  func testEquatable() {
    XCTAssert(caseOne.id == 0)
    XCTAssert(caseOne.age == 1)
    XCTAssert(caseOne.text == "a")
    XCTAssert(caseOne.desc == "b")
    XCTAssert(caseOne.images.isEmpty)

    XCTAssert(caseTwo.id == 1)
    XCTAssert(caseTwo.age == 2)
    XCTAssert(caseTwo.text == "d")
    XCTAssert(caseTwo.desc == "e")
    XCTAssert(caseTwo.images.isEmpty)

    XCTAssert(caseOne != caseTwo)
    XCTAssert(caseOne == caseOne)
  }

  func testDescribable() {
    let outputOne = caseOne.description
    XCTAssert(outputOne.contains("class: Case"))
    XCTAssert(outputOne.contains("id: 0"))
    XCTAssert(outputOne.contains("age: 1"))
    XCTAssert(outputOne.contains("text: a"))
    XCTAssert(outputOne.contains("desc: b"))
    XCTAssert(outputOne.contains("images: []"))

    let outputTwo = caseTwo.description
    XCTAssert(outputOne.contains("class: Case"))
    XCTAssert(outputTwo.contains("id: 1"))
    XCTAssert(outputTwo.contains("age: 2"))
    XCTAssert(outputTwo.contains("text: d"))
    XCTAssert(outputTwo.contains("desc: e"))
    XCTAssert(outputTwo.contains("images: []"))

    XCTAssert(outputOne != outputTwo)
  }

}
