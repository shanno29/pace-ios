import XCTest
@testable import Pace

class CaseSelectTest: XCTestCase {
  
  var vc: CaseSelect!
  
  override func setUp() {
    vc = CaseSelect()
    vc.breadCrumbsView = nil
    vc.itemCollectionView = nil
    vc.items = []
    vc.crumbs = []
    vc.title = "TestTitle"
    _ = vc.view
  }
  
  func testViewDidLoad() {
    XCTAssert(vc.items.count == 0)
    XCTAssert(vc.crumbs.count == 0)
    XCTAssert(vc.title == "TestTitle")
  }
  
  func testViewWillDisappear() {
    vc.crumbs = ["a", "b", "c"]
    XCTAssert(vc.crumbs.count == 3)
    
    vc.viewWillDisappear(false)
    vc.viewDidDisappear(true)
    XCTAssert(vc.crumbs.count == 2)
  }
  
}
