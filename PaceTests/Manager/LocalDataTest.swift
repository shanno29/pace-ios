import XCTest
@testable import Pace

class LocalDataTest: XCTestCase {

  let DATA = "data"
  let AGES = "ages"
  let CASES = "cases"
  let OPTIONS = "options"
  var localData: LocalData!

  override func setUp() {
    super.setUp()

    let ageData: [String : Any?] = ["id": 1, "text": "0 months to 3 months"]
    let caseData: [String : Any?] = ["id": 1, "age": 1, "text": "Duodenal Web", "description": "", "images": []]
    let optionData: [String : Any?] = ["id": 1, "text": "Bilious", "caption": "", "age": 1, "type": 1, "parent": nil, "cases": [], "options": []]

    localData = LocalData(data: [
      AGES: [ageData],
      CASES: [caseData],
      OPTIONS: [optionData]
    ])
  }

  /*
   * AGE
   */

  func testGetAgeById() {
    let testAge = localData.getAgeBy(id: 1)
    XCTAssert(testAge!.text == "0 months to 3 months")

    let failAge = localData.getAgeBy(id: 7)
    XCTAssert(failAge == nil)
  }

  func testGetAgeByText() {
    let testAge = localData.getAgeBy(text: "0 months to 3 months")
    XCTAssert(testAge!.id == 1)

    let failAge = localData.getAgeBy(text: "asdf")
    XCTAssert(failAge == nil)
  }

  func testGetAllAges() {
    let testAges = localData.getAllAges()
    XCTAssert(testAges.count != 0)
  }

  /*
   * CASE
   */

  func testGetCaseById() {
    let testCase = localData.getCaseBy(id: 1)
    XCTAssert(testCase?.text == "Duodenal Web")

    let failCase = localData.getCaseBy(id: 333)
    XCTAssert(failCase == nil)
  }

  func testGetCasesByIds() {
    let testCases = localData.getCasesBy(ids: [1])
    XCTAssert(testCases.count == 1)

    let failCases = localData.getCasesBy(ids: [])
    XCTAssert(failCases.count == 0)
  }

  func testGetCaseByText() {
    let testCase = localData.getCaseBy(text: "Duodenal Web")
    XCTAssert(testCase!.id == 1)

    let failCase = localData.getCaseBy(text: "asdf")
    XCTAssert(failCase == nil)
  }

  func testGetAllCases() {
    let testCase = localData.getAllCases()
    XCTAssert(testCase.count != 0)
  }

  /*
   * OPTION
   */

  func testGetOptionById() {
    let testOption = localData.getOptionBy(id: 1)
    XCTAssert(testOption?.text == "Bilious")

    let failOption = localData.getOptionBy(id: 460)
    XCTAssert(failOption == nil)
  }

  func testGetOptionsByIds() {
    let testOptions = localData.getOptionsBy(ids: [1])
    XCTAssert(testOptions.count == 1)

    let failOptions = localData.getOptionsBy(ids: [])
    XCTAssert(failOptions.count == 0)
  }

  func testGetOptionsByAgeAndParent() {
    let testOptions = localData.getOptionsBy(age: 1, parent: nil)
    XCTAssert(testOptions.count > 0)

    let failOptions = localData.getOptionsBy(age: 7, parent: 0)
    XCTAssert(failOptions.count == 0)
  }

  func testGetAllOptions() {
    let testOption = localData.getAllOptions()
    XCTAssert(testOption.count != 0)
  }

}
