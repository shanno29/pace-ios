import UIKit

let argC = CommandLine.argc

let argV = UnsafeMutableRawPointer(CommandLine.unsafeArgv).bindMemory(
  to: UnsafeMutablePointer<Int8>.self,
  capacity: Int(CommandLine.argc)
)

let path = "XCTestConfigurationFilePath"
let uiApp = NSStringFromClass(UIApplication.self)
let realApp = NSStringFromClass(AppDelegate.self)
let testApp = NSStringFromClass(UnitTestsAppDelegate.self)
let type = ProcessInfo.processInfo.environment[path] != nil ? testApp : realApp

UIApplicationMain(argC, argV, uiApp, type)
