import UIKit

class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  static var localData: LocalData!
  static var network = NetworkData()

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

    window = UIWindow(frame: UIScreen.main.bounds)
    
    let nav = UINavigationController(rootViewController: Home())
    nav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    nav.navigationBar.barTintColor = UIColor(red:0.00, green:0.51, blue:0.78, alpha:1.0)
    nav.navigationBar.barStyle = UIBarStyle.blackOpaque
    nav.navigationBar.tintColor = UIColor.white
    nav.navigationBar.isTranslucent = false
  
    window?.rootViewController = nav
    window?.makeKeyAndVisible()
    
    return true
  }

}
