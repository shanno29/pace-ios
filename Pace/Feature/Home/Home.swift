import UIKit

class Home: UIViewController {

  override public func viewDidLoad() {
    title = ""
    updateDefinition()
  }

  func updateDefinition() {
    ViewUtils().indicator(self.rootView()!, true)
    AppDelegate.network.updateDefinition {
      ViewUtils().indicator(self.rootView()!, false)
      if let _data = $0 as? [String : Any?] { AppDelegate.localData = LocalData(data: _data) }
      else { self.present(self.uiAlertController(), animated: true) }
    }
  }

  func uiAlertController() -> UIAlertController {
    let alert = UIAlertController(title: "Network Error", message: "Please Try Again", preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Retry", style: .default) { _ in self.updateDefinition() })
    alert.addAction(UIAlertAction(title: "Close", style: .cancel))
    return alert
  }
  
  @IBAction func onDiagnoseSelected(_ sender: UIButton) {
    let _vc = AgeSelect()
    _vc.items = AppDelegate.localData!.getAllAges()
    _vc.title = "Select Age"
    goTo(vc: _vc)
  }

  @IBAction func onImageBankSelected(_ sender: UIButton) {
    let _vc = ImageBank()
    _vc.items = AppDelegate.localData!.getAllCases()
    _vc.title = "Image Bank"
    goTo(vc: _vc)
  }
  
}
