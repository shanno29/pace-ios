import UIKit

class OptionSelect: UIViewController, BaseController {

  @IBOutlet weak var breadCrumbsView: BreadCrumbsView!
  @IBOutlet weak var roundedButtonsView: RoundedButtonsView!

  var items = [BaseModel]()
  var crumbs = [String]()
  
  override public func viewDidLoad() {
    breadCrumbsView?.setup(crumbs) { self.popTo(index: $0) }
    roundedButtonsView?.setup(items) { self.onClick(item: $0) }
  }

  override func viewWillDisappear(_ animated: Bool) {
    _ = breadCrumbsView?.removeLast()
    _ = crumbs.popLast()
  }

  func onClick(item: Any) {
    let _option = item as! OptionModel
    crumbs.append(_option.text)

    if !(_option.options.isEmpty) {
      let optionVC = setupOptionVC(_option.options, crumbs)
      goTo(vc: optionVC)
    }
        
    else if !(_option.cases.isEmpty) {
      let caseVC = setupCaseVC(_option.cases, crumbs)
      goTo(vc: caseVC)
    }
    
  }

  func setupOptionVC(_ ids: [Int], _ crumbs: [String]) -> UIViewController {
    let vc = OptionSelect()
    vc.items = AppDelegate.localData!.getOptionsBy(ids: ids)
    vc.title = "Select Symptom"
    vc.crumbs = crumbs
    return vc
  }
  
  func setupCaseVC(_ ids: [Int], _ crumbs: [String]) -> UIViewController {
    let vc = CaseSelect()
    vc.items = AppDelegate.localData!.getCasesBy(ids: ids)
    vc.title = "Select Case"
    vc.crumbs = crumbs
    return vc
  }
  
}
