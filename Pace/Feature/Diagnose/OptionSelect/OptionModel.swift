import UIKit

class OptionModel: NSObject, BaseModel {

  var id: Int
  var text: String
  var caption: String
  var age: Int
  var type: Int
  var parent: Int?
  var cases = [Int]()
  var options = [Int]()

  init(data: [String : Any?]) {
    self.id = data["id"] as! Int
    self.text = data["text"] as! String
    self.caption = data["caption"] as! String
    self.age = data["age"] as! Int
    self.type = data["type"] as! Int
    self.parent = data["parent"] as? Int
    self.cases = data["cases"] as! [Int]
    self.options = data["options"] as! [Int]
  }

  override var description: String {
    return
      "\n class: OptionModel" +
      "\n id: \(id)" +
      "\n text: \(text)" +
      "\n caption: \(caption)" +
      "\n age: \(age)" +
      "\n type: \(type)" +
      "\n parent: \(parent ?? -1)" +
      "\n cases: \(cases)" +
      "\n options: \(options)"
  }

  static func == (lhs: OptionModel, rhs: OptionModel) -> Bool {
    return
      lhs.id == rhs.id &&
      lhs.text == rhs.text &&
      lhs.caption == rhs.caption &&
      lhs.age == rhs.age &&
      lhs.type == rhs.type &&
      lhs.parent == rhs.parent &&
      lhs.cases == rhs.cases &&
      lhs.options == rhs.options
  }

}
