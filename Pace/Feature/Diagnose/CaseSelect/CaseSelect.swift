import UIKit

class CaseSelect: UIViewController, BaseController {

  @IBOutlet weak var breadCrumbsView: BreadCrumbsView?
  @IBOutlet weak var itemCollectionView: ItemCollectionView?

  var items = [BaseModel]()
  var crumbs = [String]()
  
  override public func viewDidLoad() {
    breadCrumbsView?.setup(crumbs) { self.popTo(index: $0) }
    itemCollectionView?.setup(items) { self.onClick(item: $0, image: $1) }
  }

  override func viewWillDisappear(_ animated: Bool) {
    _ = breadCrumbsView?.removeLast()
    _ = crumbs.popLast()
  }

  func onClick(item: Any, image: UIImage) {
    let _case = item as! CaseModel
    
    let imageVC = setupImageVC(_case.text, image)
    goTo(vc: imageVC)
  }

  func setupImageVC(_ text: String, _ image: UIImage) -> UIViewController {
    let vc = ImageSelect()
    vc.title = text
    vc.image = image
    return vc
  }
  
}
