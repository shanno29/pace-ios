import UIKit

class CaseModel: NSObject, BaseModel {

  var id: Int
  var age: Int
  var text: String
  var desc: String
  var images = [String]()

  init(data: [String : Any?]) {
    self.id = data["id"] as! Int
    self.age = data["age"] as! Int
    self.text = data["text"] as! String
    self.desc = data["description"] as! String
    self.images = data["images"] as! [String]
  }

  override var description: String {
    return
      "\n class: CaseModel" +
      "\n id: \(id)" +
      "\n age: \(age)" +
      "\n text: \(text)" +
      "\n desc: \(desc)" +
      "\n images: \(images)"
  }

  static func == (lhs: CaseModel, rhs: CaseModel) -> Bool {
    return
      lhs.id == rhs.id &&
      lhs.age == rhs.age &&
      lhs.text == rhs.text &&
      lhs.desc == rhs.desc &&
      lhs.images == rhs.images
  }

}
