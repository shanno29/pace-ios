import UIKit

class AgeModel: NSObject, BaseModel {

  var id: Int
  var text: String

  init(data: [String : Any?]) {
    self.id = data["id"] as! Int
    self.text = data["text"] as! String
  }

  override var description: String {
    return
      "\n class: AgeModel" +
      "\n id: \(id)" +
      "\n text: \(text)"
  }

  static func == (lhs: AgeModel, rhs: AgeModel) -> Bool {
    return
      lhs.id == rhs.id &&
      lhs.text == rhs.text
  }

}
