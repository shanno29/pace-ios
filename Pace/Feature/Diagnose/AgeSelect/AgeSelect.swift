import UIKit

class AgeSelect: UIViewController, BaseController {

  @IBOutlet weak var breadCrumbsView: BreadCrumbsView?
  @IBOutlet weak var roundedButtonsView: RoundedButtonsView?

  var items = [BaseModel]()
  var crumbs = [String]()

  
  override func viewDidLoad() {
    breadCrumbsView?.setup(crumbs) { self.popTo(index: $0) }
    roundedButtonsView?.setup(items) { self.onClick(item: $0) }
  }

  override func viewWillDisappear(_ animated: Bool) {
    _ = breadCrumbsView?.removeLast()
    _ = crumbs.popLast()
  }

  func onClick(item: Any) {
    let _age = item as! AgeModel
    crumbs.append(_age.text)
    
    let optionVC = setupOptionVC(_age.id, crumbs)
    goTo(vc: optionVC)
  }

  func setupOptionVC(_ id: Int, _ crumbs: [String]) -> UIViewController {
    let vc = OptionSelect()
    vc.items = AppDelegate.localData!.getOptionsBy(age: id, parent: nil)
    vc.title = "Select Symptom"
    vc.crumbs = crumbs
    return vc
  }
  
}
