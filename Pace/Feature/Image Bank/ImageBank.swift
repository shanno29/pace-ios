import UIKit

class ImageBank: UIViewController {

  @IBOutlet weak var tableView: UITableView!

  let headerHeight: CGFloat = 30
  var items = [BaseModel]()
  var data = [Int: [CaseModel]]()
  let id = ImageBankCellView.reuseIdentifier

  override public func viewDidLoad() {
    sortItems(items)
    setupTable()
  }

  func sortItems(_ list: Any) {
    if let items = list as? [CaseModel] {
      data[0] = items.filter { $0.age == 1}
      data[1] = items.filter { $0.age == 2}
      data[2] = items.filter { $0.age == 3}
      data[3] = items.filter { $0.age == 4}
      data[4] = items.filter { $0.age == 5}
      data[5] = items.filter { $0.age == 6}
    }
  }
  
  func labelText(_ section: Int) -> String {
    var res: String
    switch section {
      case 00: res = "0 months to 3 months"
      case 01: res = "3 months to 12 months"
      case 02: res = "12 months to 3 years"
      case 03: res = "3 years to 12 years"
      case 04: res = "12 years to 18 years male"
      case 05: res = "12 years to 18 years female"
      default: res = ""
    }
    return res
  }
  
  func setupTable() {
    let xib = UINib(nibName: id, bundle: nil)
    tableView.register(xib, forCellReuseIdentifier: id)
    tableView.delegate = self
    tableView.dataSource = self
    tableView.reloadData()
  }

}

extension ImageBank: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let cell = tableView.cellForRow(at: indexPath) as! ImageBankCellView
    let _vc = ImageSelect()
    _vc.image = cell.imageHolder.image
    _vc.title = cell.textHolder.text
    goTo(vc: _vc)
  }
}

extension ImageBank: UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int { return data.count }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return data[section]!.count }
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat { return headerHeight }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
    let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: headerHeight))
    view.backgroundColor = UIColor(red:0.02, green:0.65, blue:1.00, alpha:1.0)
    
    let label = UILabel(frame: CGRect(x: 15, y: 0, width: tableView.bounds.width - 30, height: headerHeight))
    label.font = UIFont.systemFont(ofSize: 12.0, weight: UIFontWeightMedium)
    label.textColor = UIColor.white
    label.text = labelText(section)
    
    view.addSubview(label)
    return view
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: id, for: indexPath)
    (cell as! ImageBankCellView).configure(data[indexPath.section]![indexPath.item], at: indexPath)
    return cell
  }
  
}
