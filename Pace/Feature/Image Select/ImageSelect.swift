import UIKit

class ImageSelect: UIViewController {

  @IBOutlet weak var imageHolder: UIImageView!
  var image: UIImage!
  
  override func viewDidLoad() {
    imageHolder.image = image
  }
  
}
