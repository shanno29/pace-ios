import UIKit

protocol BaseController {
  var items: [BaseModel] { get set }
  var crumbs: [String] { get set }
}

extension UIViewController {

  func goTo(vc: UIViewController) {
    vc.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    navigationController?.pushViewController(vc, animated: true)
  }

  func popTo(index: Int) {
    if let vc = navigationController?.viewControllers[index] {
      navigationController?.popToViewController(vc, animated: true)
    }
  }

  func rootView() -> UIView? {
    return navigationController?.view
  }

}
