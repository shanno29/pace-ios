import UIKit

@IBDesignable
class BaseView: UIView {

  var view: UIView!

  override init(frame: CGRect) {
    super.init(frame: frame)
    xibSetup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    xibSetup()
  }

  override func prepareForInterfaceBuilder() {
    super.prepareForInterfaceBuilder()
    xibSetup()
  }
    
}

extension UIView {
  
  func xibSetup() {
    let view = loadFromNib()
    view.frame = bounds
    addSubview(view)
    stretch(view: view)
  }
  
  func loadFromNib<T: UIView>() -> T {
    let selfType = type(of: self)
    let bundle = Bundle(for: selfType)
    let nibName = String(describing: selfType)
    let nib = UINib(nibName: nibName, bundle: bundle)
    let view = nib.instantiate(withOwner: self, options: nil).first as! T
    return view
  }
  
  func stretch(view: UIView) {
    view.translatesAutoresizingMaskIntoConstraints = false
      addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[childView]|", options: [], metrics: nil, views: ["childView": view]))
      addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[childView]|", options: [], metrics: nil, views: ["childView": view]))
  }

}
