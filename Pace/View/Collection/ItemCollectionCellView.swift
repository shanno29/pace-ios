import UIKit

class ItemCollectionCellView: UICollectionViewCell, ConfigurableCell {
  
  @IBOutlet weak var textHolder: UILabel!
  @IBOutlet weak var imageHolder: UIImageView!
  let PLACEHOLDER = "http://www.turbogfx.com/wp-content/uploads/tutorials/progress_bar/2.jpg"

  func configure(_ item: CaseModel, at indexPath: IndexPath) {
    textHolder.text = item.text
    AppDelegate.network.getImage(item.images.first ?? PLACEHOLDER) { self.imageUpdate($0) }
  }

  override func prepareForReuse() {
    AppDelegate.network.getImage(PLACEHOLDER) { self.imageUpdate($0) }
  }
  
  func imageUpdate(_ image: Any?) {
    if let image = image as? UIImage {
      self.imageHolder.image = image
    }
  }

}
