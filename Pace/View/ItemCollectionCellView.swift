import UIKit

class ItemCollectionCellView: UICollectionViewCell {
  
  @IBOutlet weak var imageHolder: UIImageView!
  @IBOutlet weak var textHolder: UILabel!
  var placeHolder: UIImage!
  
  func apply(_ item: CaseModel) {
    textHolder.text = item.text
    let url = (item.images.isEmpty) ? Constant.PLACEHOLDER : item.images[0]
    AppDelegate.network.getImage(url) { self.imageHolder.image = $0 as? UIImage }
  }
  
  override func prepareForReuse() {
    AppDelegate.network.getImage(Constant.PLACEHOLDER) { self.imageHolder.image = $0 as? UIImage }
    
  }

}
