import UIKit

@IBDesignable
class ItemCollectionView: BaseView {
  
  @IBOutlet weak var collectionView: UICollectionView!
  
  var items: [Any]!
  var clickHandler: ((Any, UIImage) -> Void)?
  
  func setup(_ items: [Any], _ handler: ((Any, UIImage) -> Void)? ) {
    self.items = items
    self.clickHandler = handler
    self.setupView()
  }
 
  func setupView() {
    let xib = UINib(nibName: Constant.COLLECTION, bundle: nil)
    collectionView.register(xib, forCellWithReuseIdentifier: Constant.COLLECTION)
    collectionView.delegate = self
    collectionView.dataSource = self
  }
  
}

extension ItemCollectionView: UICollectionViewDelegate {
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let cell = collectionView.cellForItem(at: indexPath)! as! ItemCollectionCellView
    self.clickHandler?(items[indexPath.item] as! CaseModel, cell.imageHolder.image!)
  }
  
}

extension ItemCollectionView: UICollectionViewDataSource {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return items.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.COLLECTION, for: indexPath) as! ItemCollectionCellView
    cell.apply(items[indexPath.item] as! CaseModel)
    return cell
  }
  
}

extension ItemCollectionView: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: CGFloat((collectionView.frame.size.width / 2) - 10), height: CGFloat(200))
  }
  
}
