import UIKit

@IBDesignable
class RoundedButtonsView: BaseView {

  @IBOutlet var buttons: [UIButton]!

  var items: [BaseModel]!
  var clickHandler: ((Any) -> Void)?

  func setup(_ items: [BaseModel], _ handler: ((Any) -> Void)? ) {
    self.items = items
    self.clickHandler = handler
    self.setupButtons()
  }

  func setupButtons() {
    for (i, btn) in buttons.enumerated() {
      if i >= items.count { btn.removeFromSuperview() }
      else { btn.setTitle(items[i].text, for: .normal) }
    }
  }

  @IBAction func onButtonClick(_ sender: UIButton) {
    let _selection = items[buttons.index(of: sender)!]
    self.clickHandler?(_selection)
  }

}
