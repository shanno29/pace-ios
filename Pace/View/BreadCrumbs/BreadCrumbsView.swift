import UIKit

@IBDesignable
class BreadCrumbsView: BaseView {

  @IBOutlet weak var tableView: UITableView!
  
  var items = [String]()
  var clickHandler: ((Int) -> Void)?
  let id = BreadCrumbsCellView.reuseIdentifier

  func setup(_ items: [String], _ handler: ((Int) -> Void)?  ) {
    self.items = items
    self.clickHandler = handler
    self.setupTable()
  }

  func setupTable() {
    let xib = UINib(nibName: id, bundle: nil)
    tableView.register(xib, forCellReuseIdentifier: id)
    tableView.delegate = self
    tableView.dataSource = self
    tableView.reloadData()
    tableView.flashScrollIndicators()
    scrollToLastCrumb()
  }

  func scrollToLastCrumb() {
    if items.count > 0 {
      let index = IndexPath(row: items.endIndex - 1, section: 0)
      tableView.scrollToRow(at: index, at: .bottom, animated: true)
      let cell = tableView.cellForRow(at: index)! as! BreadCrumbsCellView
      cell.backgroundColor = UIColor(red:0.02, green:0.65, blue:1.00, alpha:1.0)
      cell.textHolder.textColor = UIColor.white
    }
  }

  func removeLast() -> [String] {
    _ = items.popLast()
    return items
  }

}

extension BreadCrumbsView: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.clickHandler?(indexPath.item + 1)
  }
}

extension BreadCrumbsView: UITableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int { return 1 }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return items.count }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: id, for: indexPath)
    (cell as! BreadCrumbsCellView).configure(items[indexPath.item], at: indexPath)
    return cell
  }

}
