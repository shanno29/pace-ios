import UIKit

class BreadCrumbsCellView: UITableViewCell, ConfigurableCell {

  @IBOutlet weak var textHolder: UILabel!
  
  func configure(_ item: String, at indexPath: IndexPath) {
    textHolder.text = item
  }

}
