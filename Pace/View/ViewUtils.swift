import Foundation
import UIKit

class ViewUtils {

  public let id = 789456123
  
  func indicator(_ vc: UIView, _ show: Bool) {

    let container = UIView(frame: vc.frame)
    container.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    container.isUserInteractionEnabled = false
    container.center = vc.center
    container.tag = id

    let background = UIView(frame: CGRect(x:0, y:0, width:80, height:80))
    background.backgroundColor = UIColor.black.withAlphaComponent(0.7)
    background.layer.cornerRadius = 15
    background.center = vc.center

    let indicator = UIActivityIndicatorView(frame: CGRect(x:0, y:0, width:40, height:40))
    indicator.center = CGPoint(x:background.frame.size.width/2, y:background.frame.size.height/2)
    indicator.activityIndicatorViewStyle = .whiteLarge

    if !(show) { vc.viewWithTag(id)?.removeFromSuperview() }
    else {
      background.addSubview(indicator)
      container.addSubview(background)
      vc.addSubview(container)
      indicator.startAnimating()
    }

  }

}
