protocol BaseModel {

  var id: Int { get set }
  var text: String { get set }
  
}
