import UIKit

class BaseController: UIViewController {
  var items = [BaseModel]()
  var crumbs = [String]()

  override func viewDidLoad() {
    print("\n \n <~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>")
    print("[title] \(String(describing: title))")
    print("[items] \(String(describing: items)) \n")
    setBackButtonText(text: "")
  }

  func setBackButtonText(text: String) {
    let backBarButtonItem = UIBarButtonItem(title: text, style: .plain, target: nil, action: nil)
    backBarButtonItem.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.clear], for: .normal)
    backBarButtonItem.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.clear], for: .highlighted)
    navigationItem.backBarButtonItem = backBarButtonItem
  }

}

protocol Instantiable {}

extension Instantiable where Self: UIViewController {

  static func newInstance() -> Self? {
    let id = "\(self)".replacingOccurrences(of: "Controller", with: "View")
    let storyboard = UIStoryboard(name: id, bundle: nil)
    let vc = storyboard.instantiateViewController(withIdentifier: id) as? Self
    return vc
  }

  func goTo(vc: UIViewController) {
    navigationController?.pushViewController(vc, animated: true)
  }

  func popTo(index: Int) {
    if let vc = navigationController?.viewControllers[index] {
      navigationController?.popToViewController(vc, animated: true)
    }
  }

  func rootView() -> UIView? {
    return navigationController?.view
  }

}

extension UIViewController: Instantiable {}
