import UIKit

@IBDesignable
class BaseView: UIView {

  var view: UIView!

  override init(frame: CGRect) {
    super.init(frame: frame)
    xibSetup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    xibSetup()
  }

  override func prepareForInterfaceBuilder() {
    super.prepareForInterfaceBuilder()
    xibSetup()
  }
    
}

extension UIView {
  
  //1. Call this method from your init functions
  /// Helper method to init and setup the view from the Nib.
  func xibSetup() {
    let view = loadFromNib()
    view.frame = bounds
    addSubview(view)
    stretch(view: view)
  }
  
  //2. Loads the view from the nib in the bundle Method to init the view from a Nib.
  /// - Returns: Optional UIView initialized from the Nib of the same class name.
  func loadFromNib<T: UIView>() -> T {
    let selfType = type(of: self)
    let bundle = Bundle(for: selfType)
    let nibName = String(describing: selfType)
    let nib = UINib(nibName: nibName, bundle: bundle)
    let view = nib.instantiate(withOwner: self, options: nil).first as! T
    return view
  }
  
  //3. Resizes the loaded view, ready for use Stretches the input view to the UIView frame using Auto-layout
  /// - Parameter view: The view to stretch.
  func stretch(view: UIView) {
    view.translatesAutoresizingMaskIntoConstraints = false
      addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[childView]|", options: [], metrics: nil, views: ["childView": view]))
      addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[childView]|", options: [], metrics: nil, views: ["childView": view]))
  }

}
