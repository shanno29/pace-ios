import UIKit
import SwiftyJSON
import Alamofire
import AlamofireImage

class NetworkData {
  let DATA = "data"
  let API = "https://appbrewerydev.uwm.edu/pace/api/v1/init"
  typealias OnResult = (Any?) -> Void

  init() {}

  func updateDefinition(res: @escaping OnResult) {
    Alamofire.request(API).validate().responseJSON(completionHandler: {
      if $0.result.isSuccess { res(JSON($0.result.value!)[self.DATA].dictionaryObject!) }
      if $0.result.isFailure { res(nil) }
    })
  }

  func getImage(_ url: String, res: @escaping OnResult) {
    Alamofire.request(url).validate().responseImage(completionHandler: {
      if $0.result.isSuccess { res($0.result.value!) }
      if $0.result.isFailure { res(nil) }
    })
  }

}
