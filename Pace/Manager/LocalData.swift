import UIKit

final class LocalData {
  
  let AGES = "ages"
  let CASES = "cases"
  let OPTIONS = "options"
  var data: [String : Any?] = [:]

  init(data: [String : Any?]) {
    self.data[AGES] = (data[AGES] as! [[String : Any?]]).map { AgeModel(data: $0) }
    self.data[CASES] = (data[CASES] as! [[String: Any?]]).map { CaseModel(data: $0) }
    self.data[OPTIONS] = (data[OPTIONS] as! [[String: Any?]]).map { OptionModel(data: $0) }
  }
 
  func getAllAges() -> [AgeModel] {
    return data[AGES] as! [AgeModel]
  }

  func getAgeBy(id: Int) -> AgeModel? {
    return getAllAges().first { $0.id == id }
  }

  func getAgeBy(text: String) -> AgeModel? {
    return getAllAges().first { $0.text == text }
  }

  func getAllCases() -> [CaseModel] {
    return data[CASES] as! [CaseModel]
  }

  func getCaseBy(id: Int) -> CaseModel? {
    return getAllCases().first { $0.id == id }
  }

  func getCasesBy(ids: [Int]) -> [CaseModel] {
    return getAllCases().filter { ids.contains($0.id) }
  }

  func getCaseBy(text: String) -> CaseModel? {
    return getAllCases().first { $0.text == text }
  }

  func getAllOptions() -> [OptionModel] {
    return data[OPTIONS] as! [OptionModel]
  }

  func getOptionBy(id: Int) -> OptionModel? {
    return getAllOptions().first { $0.id == id }
  }

  func getOptionsBy(ids: [Int]) -> [OptionModel] {
    return getAllOptions().filter { ids.contains($0.id) }
  }

  func getOptionsBy(age: Int, parent: Int?) -> [OptionModel] {
    return getAllOptions().filter { $0.age == age && $0.parent == parent }
  }

}
